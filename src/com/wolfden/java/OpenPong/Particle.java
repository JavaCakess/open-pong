package com.wolfden.java.OpenPong;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Particle {

	private int x;
	private int y;
	private int dx;
	private int dy;
	private int size;
	private int life;
	private Color color;

	/**
	 * 
	 * @param x
	 *            - x position of the particle
	 * @param y
	 *            - y position of the particle
	 * @param dx
	 *            - x delta
	 * @param dy
	 *            - y delta
	 * @param size
	 *            - Size of the particle
	 * @param life
	 *            - Remaining lifespan of the particle
	 * @param color
	 *            - Color of the particle
	 */
	public Particle(int x, int y, int dx, int dy, int size, int life,
			Color color) {
		this.x = x;
		this.y = y;
		this.dx = dx;
		this.dy = dy;
		this.size = size;
		this.life = life;
		this.color = color;
	}

	public boolean update() {
		x += dx;
		y += dy;

		life--;
		if (life <= 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param g - Graphics
	 */
	public void render(Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.setColor(color);
		g2d.fillRect(x - (size / 2), y - (size) / 2, size, size);

		g2d.dispose();
	}

}
