package com.wolfden.java.OpenPong;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class OpenPong extends JFrame {

	private static final long serialVersionUID = 3485356756549487104L;

	private static boolean RUNNING = true;
	static long lastLoopTime = System.nanoTime();
	final static int TARGET_FPS = 60;
	final static long OPTIMAL_TIME = 1000000000 / TARGET_FPS;
	private static int fps;

	private static long lastFpsTime;

	public OpenPong() {
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setSize(800, 600);
		setUndecorated(true);
		setBackground(Color.BLACK);
		// center position
		Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screenDim.width - getWidth()) / 2,
				(screenDim.height - getHeight()) / 2);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new OpenPong().setVisible(true);
			}
		});
		startGameLoop();
	}

	private static void startGameLoop() {

		while (RUNNING) {
			// work out how long its been since the last update, this
			// will be used to calculate how far the entities should
			// move this loop
			long now = System.nanoTime();
			long updateLength = now - lastLoopTime;
			lastLoopTime = now;
			double delta = updateLength / ((double) OPTIMAL_TIME);

			// update the frame counter
			lastFpsTime += updateLength;
			fps++;

			// update our FPS counter if a second has passed since
			// we last recorded
			if (lastFpsTime >= 1000000000) {
				System.out.println("(FPS: " + fps + ")");
				lastFpsTime = 0;
				fps = 0;
			}

			// update the game logic
			// doGameUpdates(delta);

			// draw everyting
			 //render();

			// we want each frame to take 10 milliseconds, to do this
			// we've recorded when we started the frame. We add 10 milliseconds
			// to this and then factor in the current time to give
			// us our final value to wait for
			// remember this is in ms, whereas our lastLoopTime etc. vars are in
			// ns.
			try {
				Thread.sleep((lastLoopTime - System.nanoTime() + OPTIMAL_TIME) / 1000000);
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void paint(Graphics g) {
		//render(g);
	}

	public void render(Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();

		g2d.setColor(Color.WHITE);
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g2d.drawString("Great Job", 200, 200);

		g2d.dispose();
	}
}
