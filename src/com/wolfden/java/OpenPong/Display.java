package com.wolfden.java.OpenPong;

import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Window;

import javax.swing.JFrame;

public class Display {
	
	private GraphicsDevice graphicsDevice;
	
	/**
	 * @param 
	 */
	public Display(){
		GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		setGraphicsDevice(graphicsEnvironment.getDefaultScreenDevice());
	}
	
	/**
	 * @param displayMode Display Mode to set
	 * @param window Main Frame to set to fullscreen
	 */
	public void setFullScreen(DisplayMode displayMode, JFrame window){
		window.setUndecorated(true);
		window.setResizable(false);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		graphicsDevice.setFullScreenWindow(window);
		
		if(displayMode != null && graphicsDevice.isDisplayChangeSupported()){
			try{
				graphicsDevice.setFullScreenWindow(window);
				graphicsDevice.setDisplayMode(displayMode);
			}catch (Exception ex){
				
			}
		}
	}
	
	/**
	 * @return the fullscreen Window
	 */
	public Window getFullScreenWindow(){
		return graphicsDevice.getFullScreenWindow();	
	}
	
	public void restoreScreen(){
		Window window = graphicsDevice.getFullScreenWindow();
		if(window != null){
			window.dispose();
		}
		graphicsDevice.setFullScreenWindow(null);
	}

	/**
	 * @return the graphicsDevice
	 */
	public GraphicsDevice getGraphicsDevice() {
		return graphicsDevice;
	}

	/**
	 * @param graphicsDevice the graphicsDevice to set
	 */
	public void setGraphicsDevice(GraphicsDevice graphicsDevice) {
		this.graphicsDevice = graphicsDevice;
	}

}
